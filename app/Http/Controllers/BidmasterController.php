<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Project;
use App\User;
use App\Bidding;
use App\Bidetail;
use App\Vendor;
use App\ProjectDetails;


class BidmasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bidding =  Bidding::where('status','=','Saved')->get();  
      
       // $project =  Project::all();
       //   return $bidding->vebidndor;
         return view('bidmaster')->with('bids',$bidding);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return 22;
    }

    public function approve(Request $request)
    {
 
   $bidding = Bidding::find($request->bidid);
   $bidding ->status = 'Approved';
   $bidding->save();

        
        $bidding_others = Bidding::where([['project_id','=',$request->projectid],['status','=','Saved']])->get();
        
     //   return  $bidding_others;
        foreach( $bidding_others as $bidding_other)
        {


            $bidding_other ->status = 'Rejected';
           $bidding_other->save();
        }

     //  return $bidding_other;
     

     
   $project = Project::find($request->projectid);
   $project ->status = 'Awarded';
   $project->save();


   $projdtl = new Projectdetails;
   $projdtl ->vendor_id = $request->projectid; 
   $projdtl ->project_id = $request->projectid;
   $projdtl->save();
       return redirect('/bidding')->with('success','Bid Approved');


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
