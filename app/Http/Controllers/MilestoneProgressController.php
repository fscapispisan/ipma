<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Milestone;
use Illuminate\Support\Facades\DB;
use App\Task;
use Carbon\Carbon;


class MilestoneProgressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mil =  $request->input('milestone');
        $startdate =  $request->input('startdate');
        $enddate =  $request->input('enddate');
        $projid =  $request->input('projectid');
        $progress =  $request->input('progress');
      //  return  $progress;

       
DB::table('milestones')->where('project_id', '=',  $projid)->delete();

DB::table('tasks')->where('projectid', '=',  $projid)->delete();

      for ($count=0 ;  $count <  count($mil) ; $count++)
      {   

         $milestone = new Milestone;
         
         $milestone ->milestone = $mil[$count];
         $milestone ->project_id = $projid;
         $milestone ->startdate = $startdate[$count];
         $milestone ->enddate = $enddate[$count];
         $milestone ->progress = $progress[$count];
         $milestone ->status ='Approved';
    
 
         $milestone->save();

         $start = new Carbon( $startdate[$count]);
         $end = new Carbon( $enddate[$count]);

         $interval=  $start->diffInDays($end);
         $task = new Task;
         $task ->text = $mil[$count] . '   '. $progress[$count].'%';
         $task ->duration =$interval;
         $task ->progress=0;
         $task ->start_date =$startdate[$count];
         $task ->parent=0;
         $task ->projectid=$projid;

         
         $task->save();

      }
return redirect('/projectprogress')->with('success','Progress Saved');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $projects= Project::find($id);
        $milestones = Milestone::where('project_id','=',$id)->get();
//return $milestones;

   
        return view('milestoneprogress')->with('project',$projects)->with('milestones',$milestones);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
