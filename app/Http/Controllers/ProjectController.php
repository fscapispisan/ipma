<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\User;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects =  Project::all();
       //\\ return $projects->user;
        return view('home')->with('project',$projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
     $Users = User::pluck('name', 'id');
    // return $Users->id;
   
        return view('createproject')->with('Users',$Users);

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    
    {

        $this -> validate($request , [
            'projectname' => 'required',
            'projectdesc' => 'required',
            'user_id' => 'required',
         ]);


        $project = new Project;
        $project ->projectname = $request->input('projectname');
        $project ->projectdesc = $request->input('projectdesc');
        $project ->user_id = $request->input('user_id');
        $project ->status = $request->input('status');
        $project->save();

        return redirect('/home')->with('success','Post Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
           $projects= Project::find($id);
//return   $projects->user;
    
       return view('project')->with('project',$projects);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projects = Project::find($id);
     // return 123;
     
     //$Users =  User::all();

     $Users = User::pluck('name', 'id');
   
        return view('/updateproject')->with('project',$projects)->with('Users',$Users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this -> validate($request , [
            'projectname' => 'required',
            'projectdesc' => 'required',
            'user_id' => 'required',
         ]);

        $project = Project::find($id);
        $project ->projectname = $request->input('projectname');
        $project ->projectdesc = $request->input('projectdesc');
        $project ->user_id = $request->input('user_id');
        $project ->status = $request->input('status');
        $project->save();

        //return redirect('../project/',$id)->with('success','Post Updated');

        return redirect()->route('project.show',  $id)->with('success','Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projects= Project::find($id);
        $projects->delete();
        return redirect('/home')->with('success','Post Deleted');

    }
}
