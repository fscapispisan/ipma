<?php

namespace App\Http\Controllers;
use App\Project;
use App\Resource;
use App\Milestone;
use App\Billing;
use App\ProjectResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ResourceBillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $billing = new Billing;
        $billing ->projectid = $request->input('projecctid');
        $billing ->resourceid = $request->input('resourceid');
        $billing ->milestoneid = $request->input('milestone');
        $billing ->role = $request->input('role');
        $billing ->Status = 'Submitted';
       $billing->save();

       return redirect('/resourcebill/'.$request->input('projecctid'))->with('success','Billing Submitted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project= Project::find($id);
        
        $user =  Auth::user();
        
       
        $resource= Resource::where('resourceemail' ,$user->email)->first();
       
        $projresource=ProjectResource::where('resource_id' ,$resource->id)->get();
      //  return $projresource;
      $projresource = $projresource->pluck('Skills','Skills');
//return $projresource ;
        
        $milestone= Milestone::where('project_id' ,$id)->get();
        $milestone = $milestone->pluck('milestone', 'id');

       
          
        return view('resourcebilling')->with('project',$project)
             ->with('projresource',$projresource)
             ->with('milestone',$milestone);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
