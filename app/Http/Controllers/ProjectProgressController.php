<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Projectdetails;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Resource;
use App\Bidding;
use App\Biddetail;
use App\ProjectResource;

class ProjectProgressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        
        $user =  Auth::user();
        $projects = DB::table('projectdetails')
            ->join('projects', 'projects.id', '=', 'projectdetails.project_id')
            ->join('vendors', 'vendors.id', '=', 'projectdetails.vendor_id')
            ->join('resources','resources.vendor_id','=', 'vendors.id')
           //->where('resources.resourceemail','=', $user->email )
            ->select('projects.*' , 'vendors.vendorname'  )
            ->distinct()
            ->get();
          // return  $user->email;
           //    return $projects;

            return view('projectprogress')->with('project',$projects);
            //return $projects;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // return $request->input('res');
        $resource =  $request->input('res');
        $userid = $request->input('user_id');
        $project = $request->input('projectid');
     // return $resource;
     //   return count($resource);

        for ($count=0 ;  $count <  count($resource) ; $count++)
         {   

            $projres = new ProjectResource;
            $projres ->project_id = $project;
            $projres ->resource_id = $userid[$count];
            $projres ->skills = $resource[$count];
           
           $projres->save();
}


return redirect('/projectprogress')->with('success','Project Resource Completed');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $user =  Auth::user();
        
        $resource = Resource::where('resourceemail', $user->email)->first();
      //  return    $user->email;
        $bidresult = Bidding::where([['project_id','=',$id],['vendor_id','=',$resource->vendor_id]])->first();
      


        $projects= Project::find($bidresult->project_id);

      //  return $bidresult ;
        $biddtl = Biddetail::where('bidding_id','=',$bidresult->id)->get();

        $Users = Resource::where('vendor_id','=',$resource->vendor_id)->get()  ;
       // return $Users;
     $Users = $Users->pluck('resourcename', 'id');
     //return $Users;

     $projres = ProjectResource::where('project_id','=',$id);
  
if($projres->count() ==0)
    return view('resourceallocation')->with('project',$projects)->with('biddtls',$biddtl)->with('Users',$Users);
else
{

    
        
    $user =  Auth::user();
    //return  $user;
            $biddtl = DB::table('project_resources')
                ->join('projects', 'projects.id', '=', 'project_resources.project_id')
                ->join('resources','resources.id','=', 'project_resources.resource_id')
                ->where('project_resources.project_id','=', $id )
                ->select( 'projects.*','project_resources.*','resources.resourcename')
                ->get();
   // return $biddtl;
                return view('resourceallocationshow')->with('project',$projects)->with('biddtls',$biddtl)->with('Users',$Users);
    
}
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
