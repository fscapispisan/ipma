<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Milestone;
use App\Task;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MilestoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $mil =  $request->input('milestone');
        $startdate =  $request->input('startdate');
        $enddate =  $request->input('enddate');
        $projid =  $request->input('projectid');
      //  return  $projid;

       
DB::table('milestones')->where('project_id', '=',  $projid)->delete();

DB::table('tasks')->where('projectid', '=',  $projid)->delete();

      for ($count=0 ;  $count <  count($mil) ; $count++)
      {   

         $milestone = new Milestone;
         
         $milestone ->milestone = $mil[$count];
         $milestone ->project_id = $projid;
         $milestone ->startdate = $startdate[$count];
         $milestone ->enddate = $enddate[$count];
         $milestone ->status ='Saved';
         $milestone ->progress =0;
        
       // return $start;
      //   $start = date("Y-m-d",strtotime($startdate[$count]));
         
       //  $end = date("Y-m-d",strtotime($enddate[$count]));
        // return $_newDate;

         $milestone->save();
         $start = new Carbon( $startdate[$count]);
         $end = new Carbon( $enddate[$count]);

         $interval=  $start->diffInDays($end);

         $task = new Task;
         $task ->text = $mil[$count] . '   '. '0'.'%';
         $task ->duration =$interval;
         $task ->progress=0;
         $task ->start_date =$startdate[$count];
         $task ->parent=0;
         $task ->projectid=$projid;

         
         $task->save();



      }
return redirect('/projectprogress')->with('success','Milestone Saved');

      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $projects= Project::find($id);
        $milestones = Milestone::where('project_id','=',$id)->get();
       // return $milestones;

       if ( count($milestones) == 0 )       
        return view('milestone')->with('project',$projects);
        else
        return view('milestoneshow')->with('project',$projects)->with('milestones',$milestones);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $projects= Project::find($id);
        $milestones = Milestone::where('project_id','=',$id)->get();
       // return $milestones;

      
        return view('milestoneedit')->with('project',$projects)->with('milestones',$milestones);
    }

    public function approve($id)

    {

       
       DB::table('milestones')
       ->where('project_id',$id)
       ->update(['status' => 'Approved']);


        
       
       DB::table('projects')
       ->where('id', $id)
       ->update(['status' => 'In Progress']);


       return redirect('milestone/'.$id)->with('success','Milestone Approved');
       
 
    }


    public function submitted(Request $request)

    {
 
       
       DB::table('milestones')
        ->where('project_id', $request->projectid)
        ->update(['status' => 'Submitted']);


        

       return redirect('../milestone/'. $request->projectid)->with('success','Milestone Sunmitted');

 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
  
}
