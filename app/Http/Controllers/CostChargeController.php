<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Billing;

use Illuminate\Support\Facades\DB;

class CostChargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

       // $billing =  Billing::where('projectid',$id)->get();
         $billing = DB::table('billings')
         ->join('projects', 'projects.id', '=', 'billings.projectid')
         ->join('milestones', 'milestones.id', '=', 'billings.milestoneid')
         ->join('biddings','biddings.project_id', '=', 'billings.projectid')
         ->join('biddetails', [['biddings.id', '=', 'biddetails.bidding_id'],['biddetails.resource' ,'billings.role']])
         ->where('biddings.status','Approved' )
         ->select('billings.role' , 'billings.status','milestones.milestone' , 'biddetails.cost' )
            ->distinct()
            ->get();

        return view('accounting')->with('billings',$billing);
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
