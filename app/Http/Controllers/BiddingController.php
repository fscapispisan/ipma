<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\User;
use App\Bidding;
use App\Resource;
use App\Biddetail;
use Egulias\EmailValidator\Exception\CRLFAtTheEnd;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class BiddingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects =  Project::where('status','<>','In Progress')->get();
        //\\ return $projects->user;
         return view('bidding')->with('project',$projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $user =  Auth::user();
        
        $resource = Resource::where('resourceemail', $user->email)->first();
        $bidding = new Bidding;
        $bidding ->project_id = $request->input('projectid');
        $bidding ->vendor_id = $resource->vendor_id;
        $bidding ->resource_id = $user->id;
        $bidding ->status = 'Saved';
        $bidding->save();

        $bid = Bidding::where([['project_id','=',$request->input('projectid')]
        ,['vendor_id','=',$resource->vendor_id]])->first();
       
        $resource =  $request->input('resource');
        $headcount = $request->input('hdcount');
        $manday = $request->input('mandays');
        $cost = $request->input('cost');

       // return count($resource);

        for ($count=0 ;  $count <  count($resource) ; $count++)
         {   

            $biddtl = new Biddetail;
            $biddtl ->bidding_id = $bid->id;
            $biddtl ->resource = $resource[$count];
            $biddtl ->headcount = $headcount[$count];
            $biddtl ->manday = $manday[$count];
            $biddtl ->cost = $cost[$count];
    
            $biddtl->save();
}
       

       return redirect('/bidding')->with('success','Bid Submitted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
     
        $user =  Auth::user();
        
        $resource = Resource::where('resourceemail', $user->email)->first();

        $bidresult = Bidding::where([['project_id','=',$id],['vendor_id','=',$resource->vendor_id]])->first();
   
 //return $bidresult;

        $projects= Project::find($id);

$count = $bidresult->count();

if ($count == 0 ) 
    
      return view('biddetail')->with('project',$projects);
      //return 111;
       else {
        $biddtl = Biddetail::where('bidding_id','=',$bidresult->id)->get();
        $bid_count =  $biddtl ->count();

        if ($bid_count==0)
            return view('biddetail')->with('project',$projects);
        else
            return view('biddetailshow')->with('project',$projects)->with('biddtls',$biddtl);;

       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function accept($id)

    {

       DB::table('projects')
       ->where('id',$id)
       ->update(['status' => 'Invitation Accepted']);
       return redirect('/bidding')->with('success','Invitation Accepted');

    }
}
