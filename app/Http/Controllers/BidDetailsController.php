<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biddetail;
use App\Bidding;
use App\Resource;
use App\Project;
use App\User;
use Egulias\EmailValidator\Exception\CRLFAtTheEnd;
use Illuminate\Support\Facades\Auth;

class BidDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $bids= Biddetail::where('bidding_id',$request->input('bidid'))->get();

        $bid= Biddetail::where('bidding_id',$request->input('bidid'))->first();
      // return  $bid->bidding_id   ;

        $bid_proj= Bidding::where('id', $bid->bidding_id)->first();
     
       // $bid_id = $bids->bidding_id;
      // return $request->input('bidid');
      
        foreach($bids as $bid) 
            $bid->delete();
            
            
        $user =  Auth::user();
        
   //     $resource = Resource::where('resourceemail', $user->email)->first();
       
       
        $resource =  $request->input('resource');
        $headcount = $request->input('hdcount');
        $manday = $request->input('mandays');
        $cost = $request->input('cost');


       // return count($resource);

        for ($count=0 ;  $count <  count($resource) ; $count++)
         {   

         //return  $resource[$count];
            $biddtl = new Biddetail;
            $biddtl ->bidding_id = $request->input('bidid');
            $biddtl ->resource = $resource[$count];
            $biddtl ->headcount = $headcount[$count];
            $biddtl ->manday = $manday[$count];
            $biddtl ->cost = $cost[$count];
    
            $biddtl->save();
}

return redirect('/bidding/'.$bid_proj->project_id)->with('success','Bid Updated');
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bids = Biddetail::where('id',$id)->first();
  
        $biddtls = Biddetail::where('bidding_id','=',$bids->bidding_id)->get();
        
        $bidding = Bidding::where('id',$bids->bidding_id)->first();
        $projects = Project::where('id',$bidding->project_id)->first();
     

          return view('/updatebiddetails')->with('biddtls',$biddtls)->with('projects',$projects);
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
