<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Project;
use App\User;
use App\Vendor;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $vendors =  Vendor::all();
      // 

 //  foreach (  $vendors as   $vendor)
     // {return $vendor->user;}
      return view('vendor')->with('vendors',$vendors);

    //  $vendors1 =  Vendor::First();
    //  return  $vendors1->user->name;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

 
        $user = Auth::user();
        
        return view('createvendor')->with('user',$user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this -> validate($request , [
            'VendorName' => 'required',
            'Requestedby' => 'required',
      
         ]);


        $vendor = new Vendor;
        $vendor ->user_id = $request->input('user_id');
        $vendor ->VendorName = $request->input('VendorName');
        $vendor ->Requestedby = $request->input('Requestedby');
     
        $vendor->save();

        return redirect('/vendor')->with('success','Vendor Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        
        $vendors= Vendor::find($id);

    
        return view('/vendor')->with('vendor',$vendors);
     }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor = Vendor::find($id);
      
      
           return view('/updatevendor')->with('vendor',$vendor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          
        $this -> validate($request , [
            'VendorName' => 'required',
            'Requestedby' => 'required',
      
         ]);

        
        $vendor = Vendor::find($id);
      
        $vendor ->VendorName = $request->input('VendorName');
        $vendor ->Requestedby = $request->input('Requestedby');
     
        $vendor->save();

        return redirect('/vendor')->with('success','Vendor Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor= Vendor::find($id);
        $vendor->delete();
        return redirect('/vendor')->with('success','Post Deleted');
    }
}
