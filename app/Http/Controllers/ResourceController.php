<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;
use App\Vendor;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Mail;
use App\Mail\Registered;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $resources =  Resource::all();
        
        $resources = DB::table('resources')
            ->join('vendors', 'vendors.id', '=', 'resources.vendor_id')
            ->select('resources.*' , 'vendors.VendorName')
            ->get();
   

        return view('resource')->with('resources',$resources);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendors = Vendor::pluck('VendorName', 'id');
       // return $vendors->id;
        return view('createresource')->with('vendors',$vendors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this -> validate($request , [
            'resourcename' => 'required',
            'EmailAdd' => 'required',
      
         ]);


         
        $user = new User;
        $user ->name = $request->input('resourcename');
        $user ->email = $request->input('EmailAdd');
        $user ->password =Hash::make('12345678');
        $user ->role ='Vendor';
     
        
      //s  $user->save();

        $resource = new Resource;
        $resource ->resourcename = $request->input('resourcename');
        $resource ->resourceemail = $request->input('EmailAdd');
        $resource ->vendor_id = $request->input('id');
      
     //   $resource->save();


      

        Mail::to($request->input('EmailAdd'))->send(new Registered($resource));
        return redirect('/resource')->with('success','Resource Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        $resource= Resource::find($id);
        
            
               return view('resource')->with('project',$resource);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendors = New Vendor;
        $resource= Resource::find($id);
      // return  $resource->vendor;
      //  $vendors = $resource->vendor::pluck('VendorName', 'id');
       // return $vendors;
       $vendors->id = $resource->vendor->id;
       $vendors->VendorName = $resource->vendor->VendorName;
      // return $vendors ;
     //  $vendor = $vendors::pluck('VendorName', 'id');
     //  return  $vendor;

   
    
        return view('/updateresource')->with('resource',$resource)->with('vendors',$vendors);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this -> validate($request , [
            'resourcename' => 'required',
            'EmailAdd' => 'required',
      
         ]);

            
        $resource = Resource::find($id);
     
        $resource ->resourcename = $request->input('resourcename');
        $resource ->resourceemail = $request->input('EmailAdd');
       // $resource ->vendor_id = $request->input('resource_id');
     
        $resource->save();

        return redirect('/resource')->with('success','Resource Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resource= Resource::find($id);
        $resource->delete();
        return redirect('/resource')->with('success','Resource Deleted');
    }
}
