<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biddetail extends Model
{
    
    public function Bidding()
    {
        return $this->belongsTo('App\Bidding');
    }
}
