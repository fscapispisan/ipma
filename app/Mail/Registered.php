<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Resource;

class Registered extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Resource $resource)
    {
     $this->resource = $resource;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('IPMA-Unilever@unileversupport.com')
                    ->subject('Registration Status')
        ->view('emailresourceregister')
        ->with([
            'resourceemail' => $this->resource->resourceemail,
            'resource' => $this->resource->resourcename
        ]);
        //return $this->view('view.name');
    }
}
