<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timesheet extends Model
{
    protected $primaryKey = ['user_id', 'stock_id'];
}
