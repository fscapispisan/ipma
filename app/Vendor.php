<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{


        
        protected $fillable = ['id','VendorName' ];

    
    protected $table = 'vendors';
    protected $primaryKey = 'id';
  

    public function user()
    {
        return $this->belongsTo('App\User');
    }

        
    public function resources()
    {
        return $this->hasMany('App\Resource','company_id');
    }

          
    public function biddings()
    {
        return $this->hasMany('App\Bidding');
    }
    

}
