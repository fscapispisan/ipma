<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{

   // protected $fillable = ['VendorName'];

  
    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }
}
