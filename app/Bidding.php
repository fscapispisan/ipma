<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bidding extends Model
{
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    
    public function Biddetails()
    {
        return $this->hasMany('App\Biddetail');
    }


}
