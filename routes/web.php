<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "Hello Po";
});



Route::get('/hello', function () {
    //return view('welcome');
    return "Hello Po";
});



Route::get('/gantt',function() {
    return view ('gantt');
    
    });
    
    
    

    Route::get('/logout', 'PagesController@index');

Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'PagesController@admin');
Route::get('/createproject', 'ProjectController@create');
Route::resource('/project', 'ProjectController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/vendor', 'VendorController');

Route::resource('/resource', 'ResourceController');


Route::resource('/bidding', 'BiddingController');

Route::get('/bidding/accept/{proj}', 'BiddingController@accept');


Route::get('/milestone/approve', 'MilestoneController@approve');

Route::get('/milestone/submitted', 'MilestoneController@submitted');

Route::get('/bidmaster/approve','BidmasterController@approve');

Route::resource('/bidmaster', 'BidmasterController');


Route::resource('/biddetail', 'BidDetailsController');

Route::resource('/projectprogress', 'ProjectProgressController');

Route::resource('/home', 'ProjectProgressController');


Route::resource('/milestone', 'MilestoneController');

Route::resource('/milestoneprogress', 'MilestoneProgressController');



Route::resource('/gantt', 'GanttController');


Route::get('gantt/get/{proj}', 'GanttController@get');


Route::get('/milestone/approved/{proj}', 'MilestoneController@approve');


Route::resource('/resourcebill', 'ResourceBillController');
Route::resource('/costcharge', 'CostChargeController');






