<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
 
class ResourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resources')->insert([
            ['id'=>1, 'vendor_id'=>1, 'resourcename'=>'ibm resource', 
                'resourceemail'=>'ibm@ibm.com'],
                ['id'=>2, 'vendor_id'=>2, 'resourcename'=>'accenture a', 
                'resourceemail'=>'acn@acn.com'],
                ['id'=>3, 'vendor_id'=>2, 'resourcename'=>'accenture a1', 
                'resourceemail'=>'acn1@acn.com']
           
        ]);
    }
}
