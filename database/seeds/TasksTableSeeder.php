<?php
 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
 
class TasksTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tasks')->insert([
            ['id'=>1, 'text'=>'load database', 'start_date'=>'2020-02-02', 
                'duration'=>4, 'progress'=>0, 'parent'=>0 , 'projectid' =>1],
                ['id'=>2, 'text'=>'integrate', 'start_date'=>'2020-02-06', 
                'duration'=>3, 'progress'=>0, 'parent'=>0 , 'projectid' =>1],
                ['id'=>3, 'text'=>'Test Scenario', 'start_date'=>'2020-02-09', 
                'duration'=>5, 'progress'=>0, 'parent'=>0 , 'projectid' =>1],



                ['id'=>4, 'text'=>'Database Object Migration', 'start_date'=>'2020-02-02', 
                'duration'=>4, 'progress'=>0, 'parent'=>0 , 'projectid' =>5],
                ['id'=>5, 'text'=>'SIT', 'start_date'=>'2020-02-06', 
                'duration'=>3, 'progress'=>0, 'parent'=>0 , 'projectid' =>5],
                ['id'=>6, 'text'=>'UAT', 'start_date'=>'2020-02-09', 
                'duration'=>5, 'progress'=>0, 'parent'=>0 , 'projectid' =>5],
           
        ]);
    }
}