<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
 
class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert([
            ['id'=>1, 'user_id'=>1, 'projectname'=>'MPower '
                 , 'projectdesc'=>'Integration'  , 'status'=>'In progress'],
                 ['id'=>2, 'user_id'=>1, 'projectname'=>'Velocity '
                 , 'projectdesc'=>'Integration'  , 'status'=>'Open'],
                 ['id'=>3, 'user_id'=>1, 'projectname'=>'Go Bots '
                 , 'projectdesc'=>'Integration'  , 'status'=>'Open'],
                 ['id'=>4, 'user_id'=>1, 'projectname'=>'ZBB '
                 , 'projectdesc'=>'Integration'  , 'status'=>'Open'],
                 ['id'=>5, 'user_id'=>1, 'projectname'=>'MDT Track '
                 , 'projectdesc'=>'Integration'  , 'status'=>'In progress'],
              
              
        ]);
    }
}
