<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(VendorsTableSeeder::class);
        $this->call(ResourcesTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(BiddingsTableSeeder::class);
        $this->call(BiddingDetailsTableSeeder::class);
        $this->call(ProjectDetailsTableSeeder::class);
        $this->call(MilestonesTableSeeder::class);
        $this->call(TasksTableSeeder::class);
        
    }
}
