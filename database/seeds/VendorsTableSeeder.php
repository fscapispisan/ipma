<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendors')->insert([
            ['id'=>1, 'user_id'=>1, 'VendorName'=>'IBM', 
                'Requestedby'=>'Peter  Smith'],
                ['id'=>2, 'user_id'=>1, 'VendorName'=>'Accenture', 
                'Requestedby'=>'Peter  Smith'],
                ['id'=>3, 'user_id'=>1, 'VendorName'=>'CapGemini', 
                'Requestedby'=>'Peter  Smith'],
                ['id'=>4, 'user_id'=>1, 'VendorName'=>'Cognizant', 
                'Requestedby'=>'Peter  Smith'],
           
        ]);
    }
}
