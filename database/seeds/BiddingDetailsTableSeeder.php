<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class BiddingDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('biddetails')->insert([
            ['id'=>1, 'bidding_id'=>1, 'resource'=>'DBA '
                 , 'headcount'=>1  , 'manday'=>3 , 'cost' => 60],
                 ['id'=>2, 'bidding_id'=>1, 'resource'=>'Project Manager '
                 , 'headcount'=>1  , 'manday'=>3 , 'cost' => 60],
         
                 ['id'=>3, 'bidding_id'=>1, 'resource'=>'Tester '
                 , 'headcount'=>1  , 'manday'=>3 , 'cost' => 60],
         
              
                 ['id'=>4, 'bidding_id'=>2, 'resource'=>'DBA '
                 , 'headcount'=>1  , 'manday'=>3 , 'cost' => 100],
                 ['id'=>5, 'bidding_id'=>2, 'resource'=>'Project Manager '
                 , 'headcount'=>1  , 'manday'=>3 , 'cost' => 200],
         
                 ['id'=>6, 'bidding_id'=>2, 'resource'=>'Tester '
                 , 'headcount'=>1  , 'manday'=>3 , 'cost' => 300],

                 ['id'=>7, 'bidding_id'=>5, 'resource'=>'DBA '
                 , 'headcount'=>1  , 'manday'=>3 , 'cost' => 100],
                 ['id'=>8, 'bidding_id'=>5, 'resource'=>'Project Manager '
                 , 'headcount'=>1  , 'manday'=>3 , 'cost' => 200],
         
                 ['id'=>9, 'bidding_id'=>5, 'resource'=>'Tester '
                 , 'headcount'=>1  , 'manday'=>3 , 'cost' => 300],
              
        ]);
    }
}
