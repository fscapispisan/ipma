<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BiddingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('biddings')->insert([
            ['id'=>1, 'vendor_id'=>1, 'project_id'=>1, 
                'resource_id'=>1 , 'status'=>'Approved'],
             ['id'=>2, 'vendor_id'=>2, 'project_id'=>1, 
                'resource_id'=>2 , 'status'=>'Rejected'],
                ['id'=>3, 'vendor_id'=>1, 'project_id'=>2, 
                'resource_id'=>1 , 'status'=>'Saved'],
             ['id'=>4, 'vendor_id'=>2, 'project_id'=>3, 
                'resource_id'=>2 , 'status'=>'Saved'],
                ['id'=>5, 'vendor_id'=>2, 'project_id'=>5, 
                'resource_id'=>2 , 'status'=>'Approved'],
                
        ]);
    }
}
