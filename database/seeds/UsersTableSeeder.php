<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['id'=>1, 'name'=>'Francis Capispisan', 'email'=>'capisfr@gmail.com', 
                'password'=>Hash::make('12345678') , 'role' => 'Employee'],
            ['id'=>2, 'name'=>'Lucky Kiko', 'email'=>'test@gmail.com', 
                'password'=>Hash::make('12345678') , 'role' => 'Employee'],
            ['id'=>3, 'name'=>'ibm i ', 'email'=>'ibm@ibm.com', 
                'password'=>Hash::make('12345678'), 'role' => 'Vendor'],
            ['id'=>4, 'name'=>'accenture a', 'email'=>'acn@acn.com', 
                'password'=>Hash::make('12345678'), 'role' => 'Vendor'],
                ['id'=>5, 'name'=>'accenture a1', 'email'=>'acn1@acn.com', 
                'password'=>Hash::make('12345678'), 'role' => 'Vendor']
           
           
        ]);
    }
}
