<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('projectdetails')->insert([
            ['id'=>1, 'vendor_id'=>1, 'project_id'=>1],
               
            ['id'=>2, 'vendor_id'=>2, 'project_id'=>5],
              
        ]);
    }
}
