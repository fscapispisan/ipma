<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MilestonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('milestones')->insert([
            ['id'=>1, 'project_id'=>1 , 'milestone'=> 'load database',  'progress'=>  0 
            ,'status' => 'Approved', 'startdate'=>  '2020-02-04' , 'enddate'=>  '2020-02-06'],
               
            ['id'=>2, 'project_id'=>1 , 'milestone'=> 'integrate ',  'progress'=>  0 
            ,'status' => 'Approved', 'startdate'=>  '2020-02-06' , 'enddate'=>  '2020-02-09'],

                  
            ['id'=>3, 'project_id'=>1 , 'milestone'=> 'Test Scenario ',  'progress'=>  0 
            ,'status' => 'Approved', 'startdate'=>  '2020-02-09' , 'enddate'=>  '2020-02-14'],


            ['id'=>4, 'project_id'=>5 , 'milestone'=> 'Data object Migration',  'progress'=>  0 
            ,'status' => 'Approved', 'startdate'=>  '2020-02-04' , 'enddate'=>  '2020-02-06'],
               
            ['id'=>5, 'project_id'=>5 , 'milestone'=> 'SIT ',  'progress'=>  0 
            ,'status' => 'Approved', 'startdate'=>  '2020-02-06' , 'enddate'=>  '2020-02-09'],

                  
            ['id'=>6, 'project_id'=>5 , 'milestone'=> 'UAT',  'progress'=>  0 
            ,'status' => 'Approved', 'startdate'=>  '2020-02-09' , 'enddate'=>  '2020-02-14'],
        ]);
    }
}
