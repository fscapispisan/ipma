<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBiddetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biddetails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('bidding_id');
            $table->string('resource');
            $table->integer('headcount');
            $table->integer('manday');
            $table->integer('cost');
            $table->timestamps();
            $table->foreign('bidding_id')->references('id')->on('biddings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biddetails');
    }
}
