@extends('layouts.master')

@section('content')


<div class="container">
        <div class="card card-login mx-auto mt-5">


               <h1> <div class="card-header">Project Detailss</div> </h1>
                <div class="card-body">
                        {!! Form::open(['url' => ['/project/edit',$project->id], 'method'=>
                        'POST']) !!}

                        <div class="form-group">

                                        <div class="form-label-group">
                                                        {{Form::label('projectname', 'Project Name')}}
                                                </div>
                                <div class="form-label-group">
                                        {{Form::text('projectname', $project->projectname,['disabled' => 'disabled', 'class' =>  'form-control', 'placeholder' => 'Project Name'])}}
                                </div>
                        </div>

                        <div class="form-group">
                                        <div class="form-label-group">
                                                        {{Form::label('projectdesc', 'Project Descriptiom')}}
                                                </div>
                                <div class="form-label-group">
                                        {{Form::text('projectdesc',  $project->projectdesc,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Descriptiom'])}}
                                </div>
                        </div>

                        <div class="form-group">
                                <div class="form-label-group">
                                                <div class="form-label-group">
                                                                {{Form::label('owner', 'Project Manager')}}
                                                        </div>
                                        {{Form::text('owner',   $project ->user->name,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Owner'])}}
                                </div>
                        </div>


                        <div class="form-group">
                                        <div class="form-label-group">
                                                        {{Form::label('status', 'Project Status')}}
                                                </div>
                                <div class="form-label-group">
                                        {{Form::text('status',  $project->status,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Status'])}}
                                </div>
                        </div>
                      
                </div>
        </div>
        {!! Form::close() !!}


        {!! Form::open(['action' => 'BiddingController@store', 'method'=> 'POST']) !!}

<table class="table table-striped">
        <thead>
                <th>Resource</th>
                <th>Head Count</th>
                <th>Mandays</th>
                <th>Cost</th>


        </thead>
        <tbody>
                @foreach($biddtls as $biddtl)
                       
                <tr>
                        
                                     
                        <td>    {{Form::text('resource',   $biddtl->resource,['disabled'=>'disabled', 'class' =>  'form-control', 'placeholder' => 'Resource'])}}
                        </td>
                
                       
                        <td>    {{Form::text('hdcount',   $biddtl->headcount,['disabled'=>'disabled', 'class' =>  'form-control', 'placeholder' => 'Head Count'])}}
                        </td>
                        <td>    {{Form::text('mandays',   $biddtl->manday,['disabled'=>'disabled', 'class' =>  'form-control', 'placeholder' => 'Mandays'])}}
                        </td>
                        <td>    {{Form::text('cost',   $biddtl->cost,['disabled'=>'disabled', 'class' =>  'form-control', 'placeholder' => 'Cost'])}}
                        </td>
                        </tr>
                        <div class="form-label-group">
                                {{Form::hidden('bidid', $biddtl->id,['class' =>  'form-control', 'placeholder' => 'Project ID'])}}
                        </div>

                        
                    
                @endforeach
                        </table>

                        @csrf    
                       
                        <a href="../biddetail/{{ $biddtl->id}}/edit"
                                class='btn btn-primary'>Edit</a 
                                {!! Form::close() !!}

    


                </div>
@endsection