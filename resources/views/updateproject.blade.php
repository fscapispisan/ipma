@extends('layouts.master')

@section('content')


<div class="container">
        <div class="card card-login mx-auto mt-5">

                <h1>
                        <div class="card-header">Edit Project</div>
                </h1>
                <div class="card-body">
                        {!! Form::open(['action' => ['ProjectController@update',$project->id], 'method'=>
                        'POST']) !!}

                        <div class="form-group">
                                <div class="form-label-group">
                                        {{Form::text('projectname', $project->projectname,[ 'class' =>  'form-control', 'placeholder' => 'Project Name'])}}
                                </div>
                        </div>

                        <div class="form-group">
                                <div class="form-label-group">
                                        {{Form::text('projectdesc',  $project->projectdesc,['class' =>  'form-control', 'placeholder' => 'Project Description'])}}
                                </div>
                        </div>


                        <div class="form-group">
                                <div class="form-label-group">

                                 
                                        {{Form::select('user_id', $Users, $project->owner,['class' =>  'form-control'])}}


                                </div>
                        </div>


                        <div class="form-group">
                                <div class="form-label-group">
                                        {{Form::text('status',  $project->status,['class' =>  'form-control', 'placeholder' => 'Project Status'])}}
                                </div>
                        </div>
                        <div>


                                {{Form::hidden('_method','PUT')}}
                                {{Form::Submit('Save',['class'=> 'btn btn-primary'])}}
                                {!! Form::close() !!}




                        </div>

                </div>
        </div>
</div>
</div>

@endsection