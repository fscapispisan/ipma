@extends('layouts.master')



@section('content')


<div class="container">
        <div class="card card-login mx-auto mt-5">


                <h1>
                        <div class="card-header">Project Details Show</div>
                </h1>
                <div class="card-body">
                        {!! Form::open(['url' => ['/project/edit',$project->id], 'method'=>
                        'POST']) !!}

                        <div class="form-group">

                                <div class="form-label-group">
                                        {{Form::label('projectname', 'Project Name')}}
                                </div>
                                <div class="form-label-group">
                                        {{Form::text('projectname', $project->projectname,['disabled' => 'disabled', 'class' =>  'form-control', 'placeholder' => 'Project Name'])}}
                                </div>
                        </div>

                        <div class="form-group">
                                <div class="form-label-group">
                                        {{Form::label('projectdesc', 'Project Descriptiom')}}
                                </div>
                                <div class="form-label-group">
                                        {{Form::text('projectdesc',  $project->projectdesc,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Descriptiom'])}}
                                </div>
                        </div>

                        <div class="form-group">
                                <div class="form-label-group">
                                        <div class="form-label-group">
                                                {{Form::label('owner', 'Project Manager')}}
                                        </div>
                                        {{Form::text('owner',   $project ->user->name,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Owner'])}}
                                </div>
                        </div>


                        <div class="form-group">
                                <div class="form-label-group">
                                        {{Form::label('status', 'Project Status')}}
                                </div>
                                <div class="form-label-group">
                                        {{Form::text('status',  $project->status,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Status'])}}
                                </div>
                        </div>

                </div>
        </div>
        {!! Form::close() !!}


        <h1>
                <div class="card-header">Project Milestone</div>
        </h1>
        {!! Form::open(['action' => 'MilestoneProgressController@store', 'method'=> 'POST']) !!}
        <td><button type='button' name='add' id='add' class='btn btn-succss'> Add more</button></td>
       
        <table class="table table-striped" id='bidtable'>
                <thead>
                        <th>Milestone</th>
                        <th>Start Date</th>
                        <th>End date</th>
                        <th>Progress Completion</th>
                

                </thead>

                @foreach($milestones as $milestone)

        
                <tr>
                        <td> {{Form::text('milestone[]',   $milestone->milestone,[  'readonly','class' =>  'form-control', 'placeholder' => 'Milestone'])}}
                        </td>


                        <td> {{Form::date('startdate[]',    $milestone->startdate,[  'readonly','class' =>  'form-control', 'placeholder' => 'Start Date'])}}
                        </td>

                        <td> {{Form::date('enddate[]',    $milestone->enddate,[ 'readonly','class' =>  'form-control', 'placeholder' => 'End Date'])}}
                        </td>

                        <td> {{Form::text('progress[]',    $milestone->progress,['class' =>  'form-control', 'placeholder' => 'End Date'])}}
                        </td>

                        <div class="form-label-group">
                                {{Form::hidden('projectid', $project ->id,['class' =>  'form-control', 'placeholder' => 'Project ID'])}}
                        </div>
               </tr>
          

                @endforeach

             

        </table>

@csrf

        {{Form::Submit('Save',['class'=> 'btn btn-primary'])}}
        {!! Form::close() !!}

</div>


<script type='text/javascript'>
        //  alert(1);

                        window.onload = function() {
  //YOUR JQUERY CODE

                                                $(document).ready(function() {
                                                        var postURL = "<?php echo url('bidding') ?>";
                                                        var i = 1;

                                                        var html ;
                                                        $('#add').click(function (){ 
                                                                i++;

                                                        html= html  = '<tr id="row'+i+'">'
                                                        html= html + '<td><input type="text" name="milestone[]"  class="form-control" placeholder="Milestone"/> </td>';
                                                        html= html + '<td><input type="date" name="startdate[]"  class="form-control" placeholder="Resource"/> </td>';
                                                        html= html + '<td><input type="date" name="enddate[]"  class="form-control" placeholder="Resource"/> </td>';
                                                     
                                                        html= html + '<td><button name="Remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td>'
                                                        html= html + '</tr>'
                                                     
                                                              
                                                                $('#bidtable').append(html);
                                                                
                                                                //  alert(html);




                        
                                                        });


                                                        //remove

                                                        $(document).on('click' ,'.btn_remove', function(){
                                                                var button_id = $(this).attr("id");
                                                           
                                                                $('#row'+button_id+'').remove();




                                                        }
                                                        
                                                        
                                                        )
                        
                        
                        
                        
                                                });


                                        
                                        /// Action for Add and Delete Button

                                        

                                        };
                        
                        
                        
</script>


@endsection