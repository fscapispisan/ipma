@extends('layouts.master')

@section('content')

<div class="container">
        <div class="card card-login mx-auto mt-5">

                <h1>
                        <div class="card-header">Add Vendor</div>
                </h1>
                <div class="card-body">

                                {!! Form::open(['action' => ['VendorController@update',$vendor->id], 'method'=>
                                'POST']) !!}
        


                        <div class="form-group">
                                <div class="form-label-group">
                                        {{Form::label('vendor', 'Vendor Name')}}
                                </div>
                                <div class="form-label-group">
                                        {{Form::text('VendorName', $vendor->VendorName,['class' =>  'form-control', 'placeholder' => 'Vendor Name'])}}
                                </div>
                        </div>



                        <div class="form-group">

                                <div class="form-label-group">
                                        {{Form::label('vendor', 'Vendor PIC')}}
                                </div>
                                <div class="form-label-group">
                                        {{Form::text('Requestedby', $vendor->Requestedby,['class' =>  'form-control', 'placeholder' => 'Vendor PIC'])}}
                                </div>
                        </div>




     {{Form::hidden('_method','PUT')}}
                                {{Form::Submit('Save',['class'=> 'btn btn-primary'])}}
                                {!! Form::close() !!}

                </div>
        </div>
</div>
</div>


@endsection