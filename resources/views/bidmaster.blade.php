@extends('layouts.master')

@section('content')

<a href="./vendor/create"
        class='btn btn-primary'>Add Vendor company</a 
        {!! Form::close() !!}


<div class="container">
        <h2>Bidding Summary</h2>
        <div class="col-md-12">
                <div class="row">
                        <table class="table table-striped">
                                <thead>
                                        <th>Project Name</th>
                                        <th>Vendor</th>
                                        
                                        <th>Cost</th>
                                        <th>Action</th>
                                </thead>
                                <tbody>
                                        @foreach($bids as $bid)
                                        
                                        <tr>
                                                
                                        <td>{{$bid->project->projectname}}</td>
                                        <td>{{$bid->vendor->VendorName}}</td>
                                         <td>1111</td>
                                         <td>
                                                <a href="#" class="btn btn-primary"><span class="glyphicon glyphicon-pencil">VIEW</span></a>
                               

                                                


                                        {!! Form::open(['action' => 'BidmasterController@approve', 'method'=> 'GET']) !!}
                                        {{Form::hidden('bidid', $bid ->id,['class' =>  'form-control', 'placeholder' => 'bidid'])}}    
                                        {{Form::hidden('projectid', $bid ->project_id,['class' =>  'form-control', 'placeholder' => 'bidid'])}}          
                                        {{Form::hidden('vendorid', $bid ->vendor_id,['class' =>  'form-control', 'placeholder' => 'bidid'])}}          
                                       
                                        {{Form::Submit('APPROVE',['class'=> 'btn btn-primary'])}}

                                        {!! Form::close() !!}
                                                <a href="#" class="btn btn-primary"><span class="glyphicon glyphicon-pencil">REJECT</span></a>
                                         </td>
                                        </tr>

                                        @endforeach

                                </tbody>
                        </table>
                </div>
        </div>
        <h2>Bidding Detail</h2>
        <div class="col-md-12">
                <div class="row">
                        <table class="table table-striped">
                                <thead>
                                        
                                        <th>Resource</th>
                                        <th>Head Count</th>

                                        <th>Man Day</th>
                                        <th>Cost</th>
                                </thead>
                                <tbody>
                                        @foreach($bids as $bid)
                                        
                                        <tr>
                                                
                                                @foreach($bid->biddetails as $biddetail)
                                                <tr>
                                                        <td>{{$biddetail->resource}}</td>
                                              
                                                        <td>{{$biddetail->headcount}}</td>
                                                        <td>{{$biddetail->manday}}</td>
                                                        <td>{{$biddetail->cost}}</td>
                                                </tr>
                                                @endforeach
                                        </tr>

                                        @endforeach

                                </tbody>
                        </table>
                </div>
        </div>
</div>



@endsection