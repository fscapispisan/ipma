@extends('layouts.master')

@section('content')


<div class="container">
        <div class="card card-login mx-auto mt-5">


                <h1>
                        <div class="card-header">Project Details</div>
                </h1>
                <div class="card-body">


                        <div class="form-group">

                                <div class="form-label-group">
                                        {{Form::label('projectname', 'Project Name')}}
                                </div>
                                <div class="form-label-group">
                                        {{Form::text('projectname', $projects->projectname,['disabled' => 'disabled', 'class' =>  'form-control', 'placeholder' => 'Project Name'])}}
                                </div>
                        </div>

                        <div class="form-group">
                                <div class="form-label-group">
                                        {{Form::label('projectdesc', 'Project Descriptiom')}}
                                </div>
                                <div class="form-label-group">
                                        {{Form::text('projectdesc',  $projects->projectdesc,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Descriptiom'])}}
                                </div>
                        </div>

                        <div class="form-group">
                                <div class="form-label-group">
                                        <div class="form-label-group">
                                                {{Form::label('owner', 'Project Manager')}}
                                        </div>
                                        {{Form::text('owner',   $projects ->user->name,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Owner'])}}
                                </div>
                        </div>


                        <div class="form-group">
                                <div class="form-label-group">
                                        {{Form::label('status', 'Project Status')}}
                                </div>
                                <div class="form-label-group">
                                        {{Form::text('status',  $projects->status,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Status'])}}
                                </div>
                        </div>

                </div>
        </div>



        {!! Form::open(['action' => 'BidDetailsController@store', 'method'=> 'POST']) !!}

        <h1>
                <div class="card-header">Update Bidding</div>
        </h1>

        
        <tr>
                <td><button type='button' name='add' id='add' class='btn btn-primary'> Add More Rows</button></td>

        </tr>

        <table class="table table-striped" id=bidtable>

                <thead>
                        <th>Resource</th>
                        <th>Head Count</th>
                        <th>Mandays</th>
                        <th>Cost</th>


                </thead>


                @foreach($biddtls as $biddtl)

                <tr id="<?php echo $biddtl->id ?>">


                        <td> {{Form::text('resource[]',   $biddtl->resource,[ 'class' =>  'form-control', 'placeholder' => 'Resource'])}}
                        </td>


                        <td> {{Form::text('hdcount[]',   $biddtl->headcount,[ 'class' =>  'form-control', 'placeholder' => 'Head Count'])}}
                        </td>
                        <td> {{Form::text('mandays[]',   $biddtl->manday,[ 'class' =>  'form-control', 'placeholder' => 'Mandays'])}}
                        </td>
                        <td> {{Form::text('cost[]',   $biddtl->cost,[ 'class' =>  'form-control', 'placeholder' => 'Cost'])}}
                        </td>

                        <td>
                                {{Form::button('X',['id'=>$biddtl->id, 'class' =>  'btn btn-danger btn_remove', 'placeholder' => 'Cost'])}}
                        </td>
                        <td>
                                <div class="form-label-group">
                                        {{Form::hidden('bidid', $biddtl->bidding_id,['class' =>  'form-control', 'placeholder' => 'Project ID'])}}
                                </div>


                        </td>
                </tr>



                @endforeach

        </table>
        @csrf

        {{Form::Submit('Save',['class'=> 'btn btn-primary'])}}
        {!! Form::close() !!}




        <script>
                window.onload = function() {
  //YOUR JQUERY CODE

                                                $(document).ready(function() {
                                                        
                                                      
                                                        //remove

                                                        $(document).on('click' ,'.btn_remove', function(){
                                                            //    alert($(this).attr("id"));
                                                                var button_id = $(this).attr("id");
                                                           
                                                                $('#'+button_id+'').remove();

                                                        });

                                                        
                        
                        //Add

                        var i = 1;

var html ;
$('#add').click(function (){ 
        i++;


        html= html  = '<tr id="'+i+'">'
                                                        html= html + '<td><input type="text" name="resource[]"  class="form-control" placeholder="Resource"/> </td>';
                                                        html= html + '<td><input type="text" name="hdcount[]"  class="form-control" placeholder="Headcount"/> </td>';
                                                        html= html + '<td><input type="text" name="mandays[]"  class="form-control" placeholder="Manday"/> </td>';
                          
                                                        html= html + '<td><input type="text" name="cost[]"  class="form-control" placeholder="Cost"/> </td>';
                                                        html= html + '<td><button name="Remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td>'
                                                        html= html + '<td><input type="hidden" name="bidid"  class="form-control" placeholder="Manday" value = "{{ $biddtl->bidding_id}}"/> </td>';
                                                        
                                                        html= html + '</tr>'
                          
      
        $('#bidtable').append(html);

        
        //  alert(html);





});



                        




                        
                                                });


                                        
                                        /// Action for Add and Delete Button

                                        

                                        };


        </script>
        @endsection