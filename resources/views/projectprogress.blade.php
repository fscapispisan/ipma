@extends('layouts.master')

@section('content')
<h2>Working Project</h2>
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

         @if(count($project )>0)
            @foreach($project as $proj)
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ $proj->projectname}}</h3>

                            <p>{{ $proj->status}}</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="/projectprogress/{{ $proj->id}}" class="small-box-footer">Project Resource Allocation <i class="fas fa-arrow-circle-right"></i></a>
                        <a href="/milestone/{{ $proj->id}}" class="small-box-footer">Milestone Setup <i class="fas fa-arrow-circle-right"></i></a>
                        <a href="/milestoneprogress/{{ $proj->id}}" class="small-box-footer">Milestone Progress <i class="fas fa-arrow-circle-right"></i></a>
                        <a href="/gantt/{{ $proj->id}}" class="small-box-footer">Project Milestone Monitoring <i class="fas fa-arrow-circle-right"></i></a>
                        <a href="/resourcebill/{{ $proj->id}}" class="small-box-footer">Billing  <i class="fas fa-arrow-circle-right"></i></a>
                        <a href="/costcharge/{{ $proj->id}}" class="small-box-footer">Accounting  <i class="fas fa-arrow-circle-right"></i></a>
   
                    </div>
                    
                </div>
            @endforeach
        @else
                    No Projects Available
        @endif

        </div>
        <!-- /.row -->
        <!-- Main row -->

</section>
<!-- right col -->

</section>
<!-- /.content -->
@endsection