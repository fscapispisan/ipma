@extends('layouts.master')

@section('content')


<div class="container">
        <div class="card card-login mx-auto mt-5">

               <h1> <div class="card-header">Project Details</div> </h1>
                <div class="card-body">
                        {!! Form::open(['url' => ['/project/edit',$project->id], 'method'=>
                        'POST']) !!}

                        <div class="form-group">

                                        <div class="form-label-group">
                                                        {{Form::label('projectname', 'Project Name')}}
                                                </div>
                                <div class="form-label-group">
                                        {{Form::text('projectname', $project->projectname,['disabled' => 'disabled', 'class' =>  'form-control', 'placeholder' => 'Project Name'])}}
                                </div>
                        </div>

                        <div class="form-group">
                                        <div class="form-label-group">
                                                        {{Form::label('projectdesc', 'Project Descriptiom')}}
                                                </div>
                                <div class="form-label-group">
                                        {{Form::text('projectdesc',  $project->projectdesc,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Descriptiom'])}}
                                </div>
                        </div>

                        <div class="form-group">
                                <div class="form-label-group">
                                                <div class="form-label-group">
                                                                {{Form::label('owner', 'Project Manager')}}
                                                        </div>
                                        {{Form::text('owner',   $project ->user->name,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Owner'])}}
                                </div>
                        </div>


                        <div class="form-group">
                                        <div class="form-label-group">
                                                        {{Form::label('status', 'Project Status')}}
                                                </div>
                                <div class="form-label-group">
                                        {{Form::text('status',  $project->status,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Status'])}}
                                </div>
                        </div>
                      
                </div>
        </div>
        {!! Form::close() !!}
        {!! Form::open(['action' => 'ResourceBillController@store', 'method'=> 'POST']) !!}


        
        <h1> <div class="card-header">Resource Billing</div> </h1>
        <div class="card-body">
                {!! Form::open(['action' => 'ResourceBillController@store', 'method'=> 'POST']) !!}

                <div class="form-group">

                                <div class="form-label-group">
                                                {{Form::label('projectname', 'Project Role')}}
                                        </div>
                        <div class="form-label-group">
                                {{Form::select('role', $projresource,'',['class' =>  'form-control' , 'placeholder' => 'Vendor Resource'])}}
                             
                       
                        </div>
                </div>

                <div class="form-group">
                                <div class="form-label-group">
                                                {{Form::label('projectdesc', 'Hour Spent')}}
                                        </div>
                        <div class="form-label-group">
                                {{Form::text('hourspent',  0,['class' =>  'form-control', 'placeholder' => 'Hour Spent'])}}
                                {{Form::hidden('projecctid',  $project->id,['class' =>  'form-control', 'placeholder' => 'Hour Spent'])}}
                                {{Form::hidden('resourceid',   Auth::user()->id ,['class' =>  'form-control', 'placeholder' => 'Hour Spent'])}}
              
                        </div>
                </div>

                <div class="form-group">
                        <div class="form-label-group">
                                        <div class="form-label-group">
                                                        {{Form::label('owner', 'Milestone')}}
                                                </div>
                                                {{Form::select('milestone', $milestone,'',['class' =>  'form-control' , 'placeholder' => 'Milestone'])}}
                                        </div>
                </div>


                  {{Form::Submit('Submit',['class'=> 'btn btn-primary'])}}
                        {!! Form::close() !!}

        </div>
</div>
{!! Form::close() !!}
{!! Form::open(['action' => 'ProjectProgressController@store', 'method'=> 'POST']) !!}


@endsection