@extends('layouts.master')

@section('content')
<h2>Project Dashboard</h2>
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

         @if(count($projects )>0)
            @foreach($projects as $proj)
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ $proj->projectname}}</h3>

                            <p>{{ $proj->status}}</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="/project/{{ $proj->id}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            @endforeach
        @else
                    No Projects Available
        @endif

        </div>
        <!-- /.row -->
        <!-- Main row -->

</section>
<!-- right col -->

</section>
<!-- /.content -->
@endsection