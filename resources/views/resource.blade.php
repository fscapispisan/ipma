@extends('layouts.master')

@section('content')

<a href="./resource/create"
        class='btn btn-primary'>Add Resource company</a 
        {!! Form::close() !!}


<div class="container">
        <h2>Vendor Master List</h2>
        <div class="col-md-12">
                <div class="row">
                        <table class="table table-striped">
                                <thead>
                                        <th>Resource Name</th>
                                        <th>Resource Email</th>
                                        <th>Resource Company</th>
                                    

                                        <th></th>
                                </thead>
                                <tbody>
                                        @foreach($resources as $resource)
                                        <tr>
                                                <td>{{$resource->resourcename}}</td>
                                                <td>{{$resource->resourceemail}}</td>
                                                <td>{{$resource->VendorName}}</td>
                                              

                                                <td>

                                                        <a href="./resource/{{ $resource->id}}/edit"
                                                                class='btn btn-primary'>Edit</a 
                                                                {!! Form::close() !!}

                                    
                                

                                                                {!! Form::open(['action' => ['ResourceController@destroy',$resource->id],
                                                                'method'=> 'POST' ,'class'=>'float-right']) !!}
                                                                {{Form::hidden('_method','DELETE')}}
                                                                {{Form::Submit('Delete',['class'=> 'btn btn-primary'])}}
                                                                {!! Form::close() !!}
                                                                </td>
                                        </tr>
                                        @endforeach


                                </tbody>
                        </table>
                </div>
        </div>
</div>

@endsection