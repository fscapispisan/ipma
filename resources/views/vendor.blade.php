@extends('layouts.master')

@section('content')

<a href="./vendor/create"
        class='btn btn-primary'>Add Vendor company</a 
        {!! Form::close() !!}


<div class="container">
        <h2>Vendor Master List</h2>
        <div class="col-md-12">
                <div class="row">
                        <table class="table table-striped">
                                <thead>
                                        <th>Vendor Name</th>
                                        <th>Vendor PIC</th>
                                        <th>Onboarded by</th>

                                        <th></th>
                                </thead>
                                <tbody>
                                        @foreach($vendors as $vendors)
                                        <tr>
                                                <td>{{$vendors->VendorName}}</td>
                                                <td>{{$vendors->Requestedby}}</td>
                                                <td>{{$vendors->user->name}}</td>

                                                <td>

                                                        <a href="./vendor/{{ $vendors->id}}/edit"
                                                                class='btn btn-primary'>Edit</a 
                                                                {!! Form::close() !!}

                                    
                                

                                                                {!! Form::open(['action' => ['VendorController@destroy',$vendors->id],
                                                                'method'=> 'POST' ,'class'=>'float-right']) !!}
                                                                {{Form::hidden('_method','DELETE')}}
                                                                {{Form::Submit('Delete',['class'=> 'btn btn-primary'])}}
                                                                {!! Form::close() !!}
                                                                </td>
                                        </tr>
                                        @endforeach


                                </tbody>
                        </table>
                </div>
        </div>
</div>

@endsection