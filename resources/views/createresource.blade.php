@extends('layouts.master')

@section('content')

<div class="container">
        <div class="card card-login mx-auto mt-5">

                <h1>
                        <div class="card-header">Add Vendor Resource</div>
                </h1>
                <div class="card-body">

                        {!! Form::open(['action' => 'ResourceController@store', 'method'=> 'POST']) !!}

                        
                        <div class="form-group">

                                <div class="form-label-group">
                                        {{Form::label('resource', 'Resource Full Name')}}
                                </div>
                                <div class="form-label-group">
                                        {{Form::text('resourcename', '',['class' =>  'form-control', 'placeholder' => 'Resource Full Name'])}}
                                </div>
                        </div>



                        <div class="form-group">

                                        <div class="form-label-group">
                                                        {{Form::label('resource', 'Company Name')}}
                                                </div>

                                <div class="form-label-group">
                                        {{Form::select('id', $vendors,'',['class' =>  'form-control' , 'placeholder' => 'Company Name'])}}
                                </div>
                        </div>


                        <div class="form-group">
                                <div class="form-label-group">
                                        <div class="form-label-group">
                                                {{Form::label('resource', 'Email Address')}}
                                        </div>
                                        {{Form::Email('EmailAdd', '',['class' =>  'form-control', 'placeholder' => 'Email Address'])}}
                                </div>
                        </div>


               



                        {{Form::Submit('Submit',['class'=> 'btn btn-primary'])}}
                        {!! Form::close() !!}
                </div>
        </div>
</div>
</div>


@endsection