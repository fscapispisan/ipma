@extends('layouts.master')

@section('content')

<div class="container">
        <div class="card card-login mx-auto mt-5">

                <h1>
                        <div class="card-header">Create Project</div>
                </h1>
                <div class="card-body">

                        {!! Form::open(['action' => 'ProjectController@store', 'method'=> 'POST']) !!}

                        <div class="form-group">
                                       <div class="form-label-group">
                                        {{Form::label('proj', 'Project Name')}}
                                </div>
                                <div class="form-label-group">
                                        {{Form::text('projectname', '',['class' =>  'form-control', 'placeholder' => 'Project Name'])}}
                                </div>
                        </div>

                        <div class="form-group">
                                        <div class="form-label-group">
                                                        {{Form::label('proj', 'Project Description')}}
                                                </div>
                                <div class="form-label-group">
                                        {{Form::text('projectdesc', '',['class' =>  'form-control', 'placeholder' => 'Project Description'])}}
                                </div>
                        </div>


                        <div class="form-group">
                                        <div class="form-label-group">
                                                        {{Form::label('proj', 'Project Manager')}}
                                                </div>
                                <div class="form-label-group">
                                        {{Form::select('user_id', $Users,'',['class' =>  'form-control' , 'placeholder' => 'Project Managers'])}}
                                </div>
                        </div>


                        <div class="form-group">
                                        
                                <div class="form-label-group">
                                        {{Form::hidden('status', 'Open',['class' =>  'form-control', 'placeholder' => 'Project Status'])}}
                                </div>
                        </div>






                        {{Form::Submit('Submit',['class'=> 'btn btn-primary'])}}
                        {!! Form::close() !!}
                </div>
        </div>
</div>
</div>


@endsection