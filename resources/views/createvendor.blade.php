@extends('layouts.master')

@section('content')

<div class="container">
        <div class="card card-login mx-auto mt-5">

                <h1>
                        <div class="card-header">Add Vendor Company Details</div>
                </h1>
                <div class="card-body">

                        {!! Form::open(['action' => 'VendorController@store', 'method'=> 'POST']) !!}

                        <div class="form-group">

                                <div class="form-label-group">
                                        {{Form::hidden('user_id', $user->id,['class' =>  'form-control', 'placeholder' => 'Project Name'])}}
                                </div>
                        </div>

                        <div class="form-group">

                                <div class="form-label-group">
                                        {{Form::label('vendor', 'Vendor Name')}}
                                </div>
                                <div class="form-label-group">
                                        {{Form::text('VendorName', '',['class' =>  'form-control', 'placeholder' => 'Vendor Name'])}}
                                </div>
                        </div>



                        <div class="form-group">
                                <div class="form-label-group">
                                        <div class="form-label-group">
                                                {{Form::label('vendor', 'Vendor PIC')}}
                                        </div>
                                        {{Form::text('Requestedby', '',['class' =>  'form-control', 'placeholder' => 'Vendor PIC'])}}
                                </div>
                        </div>






                        {{Form::Submit('Submit',['class'=> 'btn btn-primary'])}}
                        {!! Form::close() !!}
                </div>
        </div>
</div>
</div>


@endsection