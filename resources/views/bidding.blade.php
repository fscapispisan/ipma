@extends('layouts.master')

@section('content')
<h2>Bidding Summary</h2>
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

         @if(count($project )>0)
            @foreach($project as $proj)
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{ $proj->projectname}}</h3>

                            <p>{{ $proj->status}}</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        @if(  $proj->status =="Open") 
                        <a href="/bidding/accept/{{ $proj->id}}" class="small-box-footer">Accept Invite <i class="fas fa-arrow-circle-right"></i></a>
              @else
              <a href="/bidding/{{ $proj->id}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>

                        @endif
                    </div>
                </div>
            @endforeach
        @else
                    No Projects Available
        @endif

        </div>
        <!-- /.row -->
        <!-- Main row -->

</section>
<!-- right col -->

</section>
<!-- /.content -->
@endsection