@extends('layouts.master')

@section('content')


<div class="container">
        <h2>Accounting</h2>
        <div class="col-md-12">
                <div class="row">
                        <table class="table table-striped">
                                <thead>
                                        <th>Role</th>
                                        <th>Milestone</th>
                                        <th>Cost</th>
                                        <th>Status</th>

                                </thead>
                                <tbody>
                                        @foreach($billings as $billing)
                                        <tr>
                                                <td>{{$billing->role}}</td>
                                                
                                                <td>{{$billing->milestone}}</td>
                                                
                                                <td>{{$billing->cost}}</td>
                                                <td>{{$billing->status}}</td>
                                                
                                        </tr>
                                        @endforeach


                                </tbody>
                        </table>
                </div>
        </div>
</div>

@endsection