@extends('layouts.master')



@section('content')


<div id="gantt_here" style='width:100%; height:30%;'></div>

<script type="text/javascript">

gantt.config.date_format = "%Y-%m-%d %H:%i:%s";
gantt.config.fit_tasks = true; 
gantt.config.readonly = true;
gantt.config.autosize = "y";
gantt.config.scroll_size = 20;
gantt.init("gantt_here");
gantt.load("../gantt/get/1");

</script>


@endsection