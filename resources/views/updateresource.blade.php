@extends('layouts.master')

@section('content')

<div class="container">
        <div class="card card-login mx-auto mt-5">

                <h1>
                        <div class="card-header">Edit Vendor Resource Details</div>
                </h1>
                <div class="card-body">

                        {!! Form::open(['action' => ['ResourceController@update',$resource->id], 'method'=>
                        'POST']) !!}


                        
                        <div class="form-group">

                                <div class="form-label-group">
                                        {{Form::label('resource', 'Resource Full Name')}}
                                </div>
                                <div class="form-label-group">
                                        {{Form::text('resourcename', $resource->resourcename,['class' =>  'form-control', 'placeholder' => 'Resource Full Name'])}}
                                </div>
                        </div>



                        <div class="form-group">

                                        <div class="form-label-group">
                                                        {{Form::label('resource', 'Company Name')}}
                                                </div>

                                <div class="form-label-group">
                                        {{Form::select('resource_id', [$vendors->id => $vendors->VendorName],null,['disabled' => 'disabled','class' =>  'form-control' , 'placeholder' => $vendors->VendorName])}}
                                </div>
                        </div>


                        <div class="form-group">
                                <div class="form-label-group">
                                        <div class="form-label-group">
                                                {{Form::label('resource', 'Email Address')}}
                                        </div>
                                        {{Form::Email('EmailAdd', $resource->resourceemail,['class' =>  'form-control', 'placeholder' => 'Email Address'])}}
                                </div>
                        </div>


               




                        {{Form::hidden('_method','PUT')}}
                        {{Form::Submit('Save',['class'=> 'btn btn-primary'])}}
                        {!! Form::close() !!}



                </div>
        </div>
</div>
</div>


@endsection