@extends('layouts.master')

@section('content')


<div class="container">
        <div class="card card-login mx-auto mt-5">


               <h1> <div class="card-header">Project Details</div> </h1>
                <div class="card-body">
                        {!! Form::open(['url' => ['/project/edit',$project->id], 'method'=>
                        'POST']) !!}

                        <div class="form-group">

                                        <div class="form-label-group">
                                                        {{Form::label('projectname', 'Project Name')}}
                                                </div>
                                <div class="form-label-group">
                                        {{Form::text('projectname', $project->projectname,['disabled' => 'disabled', 'class' =>  'form-control', 'placeholder' => 'Project Name'])}}
                                </div>
                        </div>

                        <div class="form-group">
                                        <div class="form-label-group">
                                                        {{Form::label('projectdesc', 'Project Descriptiom')}}
                                                </div>
                                <div class="form-label-group">
                                        {{Form::text('projectdesc',  $project->projectdesc,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Descriptiom'])}}
                                </div>
                        </div>

                        <div class="form-group">
                                <div class="form-label-group">
                                                <div class="form-label-group">
                                                                {{Form::label('owner', 'Project Manager')}}
                                                        </div>
                                        {{Form::text('owner',   $project ->user->name,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Owner'])}}
                                </div>
                        </div>


                        <div class="form-group">
                                        <div class="form-label-group">
                                                        {{Form::label('status', 'Project Status')}}
                                                </div>
                                <div class="form-label-group">
                                        {{Form::text('status',  $project->status,['disabled' => 'disabled','class' =>  'form-control', 'placeholder' => 'Project Status'])}}
                                </div>
                        </div>
                      
                </div>
        </div>
        {!! Form::close() !!}
        {!! Form::open(['action' => 'ProjectProgressController@store', 'method'=> 'POST']) !!}


        
        <h1> <div class="card-header">Project Resource</div> </h1>
<table class="table table-striped">
        <thead>
                <th>SKill</th>
                <th>Resource</th>
               

        </thead>
        <tbody>
                @foreach($biddtls as $biddtl)
                   

                    
                     
                        @foreach(range(1,$biddtl->headcount) as $val) 
                        <tr>
                        <td>    {{Form::text('resource[]',   $biddtl->resource,['disabled'=>'disabled', 'class' =>  'form-control', 'placeholder' => 'Resource'])}}
                        </td>

                        <td>  
                       
                        {{Form::select('user_id[]', $Users,'',['class' =>  'form-control' , 'placeholder' => 'Vendor Resource'])}}
            
                       
                            </td>

                            <td>    {{Form::hidden('res[]',   $biddtl->resource,[ 'class' =>  'form-control', 'placeholder' => 'Resource'])}}
                        </td>
                <tr>
                       
                        @endforeach  
                     
                        <div class="form-label-group">
                                {{Form::hidden('projectid', $project->id,['class' =>  'form-control', 'placeholder' => 'Project ID'])}}
                        </div>

                        
                    
                @endforeach
                        </table>

                        @csrf    
                       
                        
                        {{Form::Submit('Submit',['class'=> 'btn btn-primary'])}}
                        {!! Form::close() !!}

    


                </div>
@endsection